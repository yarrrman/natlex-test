import { Component, OnInit, Input, NgZone, ElementRef } from '@angular/core';
import { DiagramsService } from "../diagrams.service";

import * as  am4charts from "@amcharts/amcharts4/charts";

@Component({
  selector: 'app-diagram-list-item',
  templateUrl: './diagram-list-item.component.html',
  styleUrls: ['./diagram-list-item.component.css']
})
export class DiagramListItemComponent implements OnInit {
  private chart: am4charts.XYChart;
  private chartEl;
  data;
  @Input() card;
  nativeEl;
  
  
  constructor(private zone: NgZone, private el: ElementRef, private diagramsService: DiagramsService) { 
      this.nativeEl = el.nativeElement;
  }
    
    ngOnInit(): void {
      console.log(this.card);
    }
    
    ngAfterViewInit() {
      
      this.zone.runOutsideAngular(() => {
        this.chartEl = this.nativeEl.querySelector('.chartdiv');
        if (!this.chartEl ) return;
        this.chart = this.diagramsService.createChart(this.chartEl,  this.card.diagram);
    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  

}
