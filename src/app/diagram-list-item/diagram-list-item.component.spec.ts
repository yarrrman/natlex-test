import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagramListItemComponent } from './diagram-list-item.component';

describe('DiagramListItemComponent', () => {
  let component: DiagramListItemComponent;
  let fixture: ComponentFixture<DiagramListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagramListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagramListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
