export enum SensorType {
    Temperature,
    Humidity,
    Light,
    Barometric
}
