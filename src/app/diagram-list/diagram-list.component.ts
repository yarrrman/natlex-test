import { Component, ViewChild } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { of, Observable, from, fromEvent } from 'rxjs';

import { DiagramsService } from "../diagrams.service";
import { Diagram } from '../diagram';
import { FormControl } from '@angular/forms';

import { DateFilterService } from "../date-filter.service";

@Component({
  selector: 'app-diagram-list',
  templateUrl: './diagram-list.component.html',
  styleUrls: ['./diagram-list.component.css']
})




export class DiagramListComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards;
  /* of([
    { title: '1', cols: 1, rows: 1, diagram: {}   },
    { title: '1', cols: 1, rows: 1, diagram: {}   },
    { title: '1', cols: 1, rows: 1, diagram: {}   },
    { title: '1', cols: 1, rows: 1, diagram: {}   },
  ]); */
  
  cols = 2;
  
  length=4
  pageSize=4
  pageIndex = 0;
  pageSize$: Observable<number>;
  
  pageSizeOptions=[1,2,4];

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }
  constructor(private breakpointObserver: BreakpointObserver, private diagramsService: DiagramsService, private dateFilterService: DateFilterService) {
    this.breakpointObserver.observe(Breakpoints.Handset).subscribe(val => {
      console.log(val);
       if (val.matches) {
          this.cols = 1;
       } else {
         this.cols = 2;
        }
    });
  }

  startDate = new FormControl(this.dateFilterService.startDate);
  endDate = new FormControl(this.dateFilterService.endDate);

  ngOnInit () {
    console.log(this.diagramsService);
    this.pageIndex = 0;
    this.getCards();
  }

  onPageEvent(ev) {
    console.log('PageEvent', ev);
    this.pageIndex = ev.pageIndex;
    this.pageSize = ev.pageSize;
    this.getCards();
  }

  changeDate(type, ev) {
    console.log(type, ev, this.startDate);
    if (type=='startDate') {
      this.dateFilterService.startDate = ev.value;
      this.startDate.setValue(ev.value);
    } else {
      this.dateFilterService.endDate = ev.value;
      this.endDate.setValue(ev.value); 
    }
    this.getCards();
  }

  getCards () {
    this.diagramsService.getDiagrams(this.startDate.value, this.endDate.value).subscribe(diagrams => {
        console.log(this.pageSize, diagrams);
        this.length = diagrams.length;

        this.cards = diagrams.slice(this.pageIndex * this.pageSize,this.pageIndex * this.pageSize + this.pageSize).map((diagram:Diagram, index) => {
          return {
            title: `${diagram.sensorType} - ${diagram.date.toDateString()}`, cols: this.pageSize <= 2 ? 2  : 1, rows: 1, diagram
          }
        
      });

      
    })
  }
}
