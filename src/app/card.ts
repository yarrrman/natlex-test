import { SensorType } from './sensor-type.enum';

export class Card {
    title: string;
    sensorType: SensorType;
    diagramId: number;

    constructor (title: string, sensorType:SensorType, diagramId: number) {
        this.title = title;
        this.sensorType = sensorType;
        this.diagramId = diagramId;
    }

}
