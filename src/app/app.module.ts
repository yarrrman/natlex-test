import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DiagramComponent } from './diagram/diagram.component';

import { MaterialModule } from "./material-module";

import { ReactiveFormsModule } from "@angular/forms";

import { MatGridListModule } from "@angular/material/grid-list";
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from "@angular/material/toolbar";
import { LayoutModule } from '@angular/cdk/layout';
import { MatDatepickerModule } from "@angular/material/datepicker";
//import { MatFormFieldModule } from "@angular/material/form-field";


import { DiagramListComponent } from './diagram-list/diagram-list.component';
import { DiagramListItemComponent } from './diagram-list-item/diagram-list-item.component';



@NgModule({
  declarations: [
    AppComponent,
    DiagramComponent,
    DiagramListComponent,
    DiagramListItemComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      {path: 'dashboard', component: DiagramListComponent},
      {path: 'detailed', component: DiagramComponent}
    ]),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
