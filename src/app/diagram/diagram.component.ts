import { Component, OnInit,  NgZone, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateFilterService } from "../date-filter.service";
import { SensorTypes } from "../sensor-types";
import { DiagramsService } from "../diagrams.service";
import { Diagram } from '../diagram';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);


interface PointData {
  date: Date;
  y: Number;
}

interface Point {
  measureUnit: string;
  points: PointData[]
}


@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css']
})
export class DiagramComponent implements OnInit {

  startDate = new FormControl(this.dateFilterService.startDate);
  endDate = new FormControl(this.dateFilterService.endDate);

  sensors = []
  selectedSensor = new FormControl();
  
  selectedSensorsList: string[] = Object.keys(SensorTypes);

  diagramTypes = [];
  selectedDiagramType;

  chart;
  
  chartEl;
  
  constructor(private dateFilterService: DateFilterService, private diagramsService: DiagramsService, private zone: NgZone, private el: ElementRef) { 
    this.selectedSensorsList = Object.keys(SensorTypes);
    

    this.diagramTypes = ['lines', 'bars'];
    this.selectedDiagramType = this.diagramTypes[0];
  }

  selectDefault() {
    console.log('init');
    this.selectedSensor.setValue([this.selectedSensorsList[0]]);
    this.getDiagrams();
  }
  
  ngOnInit(): void {
    
    this.selectDefault();
  }

  ngAfterViewInit() {
  
    
  }


  changeSelect(type, ev) {
    console.log(type, ev);

    if (type == 'sensorType') {
      this.selectedSensor.setValue(ev.value);
    }

    this.getDiagrams();
  }
  
  generateDiagram(diagrams) {
      this.zone.runOutsideAngular(() => {
      
      if (!this.chartEl) this.chartEl = this.el.nativeElement.querySelector('.chartdiv');
      if (!this.chartEl ) return;

      this.chart = this.createChart(this.chartEl,  diagrams);
    });
  }

  
  getDiagrams () {
    this.diagramsService.getDiagrams(this.startDate.value, this.endDate.value).subscribe(diagramData => {
        //diagrams = diagrams.filter(d => d.sensorType == this.selectedSensor);

        let diagrams = this.selectedSensor.value.reduce((acc, val) => {
          acc[val] = {
            points: [],
            measureUnit: ''
          };
          
          console.log(diagramData);

          let diagrams = diagramData
            .filter((diagram:Diagram) => diagram.sensorType == val);

          console.log(diagrams);

          let points: PointData[] = diagrams.reduce((acc2, diagram:Diagram) => {
              acc2 = acc2.concat(
                diagram.data.map((point, idx) => {
                  return {
                    date: new Date(diagram.date).setHours(diagram.date.getHours() + idx + 1),
                    y: point,
                  };
                })
              );
              return acc2;
            }, []);

            console.log(points);
            acc[val].points = points;
            acc[val].measureUnit = diagrams[0] ? diagrams[0].measureUnit : '';
          return acc;
        }, {})

        console.log(diagrams);

        this.generateDiagram(diagrams);
    });
  }

  changeDate(type, ev) {
    console.log(type, ev, this.startDate);
    if (type=='startDate') {
      this.dateFilterService.startDate = ev.value;
      this.startDate.setValue(ev.value);
      
    } else {
      this.dateFilterService.endDate = ev.value;
      this.endDate.setValue(ev.value); 
    }
    this.getDiagrams();
  }



  createChart(el: HTMLElement, diagrams: []) {


    let chart = am4core.create(el, am4charts.XYChart);
    chart.paddingRight = 20;
    chart.colors.list = [
      am4core.color("#845EC2"),
      am4core.color("#D65DB1"),
      am4core.color("#FF6F91"),
      am4core.color("#FF9671"),
      am4core.color("#FFC75F"),
      am4core.color("#F9F871")
    ];
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;
    

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;

    Object.keys(diagrams).forEach((key) => {
      let data = [];
      let diagram: Point = diagrams[key];
      for (let i = 0; i < diagram.points.length; i++) {
        data.push({
          date: diagram.points[i].date,
          name: "name" + (i + 1),
          value: diagram.points[i].y
        })
      }
      let series = chart.series.push(this.selectedDiagramType == this.diagramTypes[0] ? new am4charts.LineSeries() : new am4charts.ColumnSeries());

      series.data = data;
      series.dataFields.dateX = 'date';
      series.dataFields.valueY = 'value';
      series.tooltipText =  key + ': {valueY.value} ' + diagram.measureUnit;

      /* let scrollbarX = new am4charts.XYChartScrollbar();
      scrollbarX.series.push(series); */
      chart.scrollbarX = new am4core.Scrollbar();
  
    });

    


  
    chart.cursor = new am4charts.XYCursor();


    return chart;
  }


}
