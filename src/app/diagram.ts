import { SensorTypes } from "./sensor-types";

export class Diagram {
    id: number;
    sensorType: string;
    data: number[];
    date: Date;
    
    public get measureUnit ():string {
        let type = '';
        switch (this.sensorType) {
            case SensorTypes.Temperature:
                type = 'C°';
                break;
            case SensorTypes.Light:
                type = 'lux';
                break;
            case SensorTypes.Humidity:
                type = '%';
                break;
            case SensorTypes["Barometric pressure"]:
                type = 'mm';
                break;
            default:
                break;
        }
        return type;
    }

    constructor (id: number, sensorType: string, data: number[], date: Date) {
        this.id = id;
        this.sensorType = sensorType;
        this.data = data;
        this.date = date;
    }
}
