import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateFilterService {
  startDate: Date;
  endDate: Date;

  constructor() {
    let curDate = new Date();
    this.endDate = (new Date( curDate.getFullYear(), curDate.getMonth(), curDate.getDate() - 1));
    this.startDate = (new Date( curDate.getFullYear(), curDate.getMonth(), curDate.getDate() - 4));
   }
}
