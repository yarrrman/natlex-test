import { Injectable } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);


import { Card } from "./card";
import { Diagram } from './diagram';
import { SensorTypes } from "./sensor-types";
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiagramsService {
  diagrams: Diagram[] = [];
  diagramNumbers;
  
  private _diagrams: Diagram[] = [];

  getDiagrams(startDate: Date = null, endDate: Date = null):Observable<any[]>  {
    this.generateRandomDiagrams();
    
    let ds = (this.diagrams.filter(d => {
        let start = !startDate ? true : d.date >= startDate;
        let end = !endDate ? true : d.date <= endDate;
        return start && end;
    }));

    return of(ds);
    
  }

  createChart(el: HTMLElement, diagram: Diagram) {
    let chart = am4core.create(el, am4charts.XYChart);
    chart.paddingRight = 20;
    let data = [];
    
    let lengths = diagram.data.length;
    
    for (let i = 0; i < diagram.data.length; i++) {
      let time = (new Date(diagram.date)).setHours(diagram.date.getHours() + i);
      data.push({
        date: time,
        name: "name" + (i + 1),
        value: diagram.data[i]
      })
    }

    chart.data = data;

    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;

    let series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = 'date';
    series.dataFields.valueY = 'value';

    series.tooltipText = '{valueY.value} ' + diagram.measureUnit;
    chart.cursor = new am4charts.XYCursor();

    //let scrollbarX = new am4charts.XYChartScrollbar();
    //scrollbarX.series.push(series);
    //let scrollbarX = new am4core.Scrollbar();
    //chart.scrollbarX = scrollbarX;

    return chart;
  }

  constructor() {}

  ngOnInit() {
    
  }

  private generateRandomDiagrams() {
    if (this._diagrams.length) {
      this.diagrams = this._diagrams;
      return;
    }
    
    let startDateYear = 2020
    let startMonth = 0;
    let startDay = 1;

    
    let diagramNumbers:number = Math.floor((+new Date()  - +new Date(startDateYear, startMonth, startDay)) / (1000 * 24 *60* 60));

    let sensorTypes = Object.keys(SensorTypes);

    for (let i = 0; i < diagramNumbers; i++) {
      for (let j = 0; j < sensorTypes.length; j++) {
        let d = new Diagram(i, sensorTypes[j] , this.generateRandomSeres(), new Date(startDateYear, startMonth, startDay + i))        
        //console.log(d);
        this.diagrams.push(d);
      }
    }
    
    this._diagrams = this.diagrams;
  }

  private generateRandomSeres() {
    let pointsNumber: number = 24;
    let points = [];
    let minRange: number = -50;
    let maxRange: number = 50;

    let getRandom = () => {
      return Math.random() * (maxRange - minRange) + minRange;
    }

    while (pointsNumber > 0) {
      points.push(getRandom());
      pointsNumber--;
    }
    return points;
  }
}
