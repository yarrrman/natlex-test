export const SensorTypes =  {
    "Temperature":"Temperature",
    "Humidity":"Humidity",
    "Light":"Light",
    "Barometric pressure":"Barometric pressure",
}
